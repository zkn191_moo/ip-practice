<section class="header">
    <div class="container">
        <div class="header-left">
            <ul class="pull-left">
                <li>
                    <a href="#">
                        <i class="fa fa-phone" aria-hidden="true"></i> 0412480220
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class="fa fa-envelope" aria-hidden="true"></i>imperiya.bud@ukr.net
                    </a>
                </li>
            </ul>
        </div>
        <div class="header-right pull-right">
            <ul>
                <li class="reg">
                    <?php if(User::isGuest()): ?>
                        <a href="/user/login/">Увійти</a>
                    <?php else: ?>
                        <a href="/cabinet/">Акаунт</a>
                        <a href="/user/logout/">Вихід</a>
                    <?php endif; ?>
                </li>
            </ul>
        </div>
    </div>
</section>

<section id="menu">
    <div class="container">
        <div class="menubar">
            <nav class="navbar navbar-default">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Навігація</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/">
                        <img src="/template/img/logo/logo.png" alt="logo" width="60">
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        <li class="active"><a href="/">Головна</a></li>
                        <li><a href="/about/">Про нас</a></li>
                        <li><a href="/contacts/">Контакти</a></li>
                        <li><a href="/projects/">Реалізовані проєкти</a></li>
                        <li><a href="/repair/">Ремонт</a></li>
                        <li><a href="/building/">Будівництво</a></li>
                        <li><a href="/architecture/">Архітектура</a></li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>
</section>