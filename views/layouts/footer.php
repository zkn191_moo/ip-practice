<section class="hm-footer">
    <div class="container">
        <div class="hm-footer-details">
            <div class="row">
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="hm-footer-widget">
                        <div class="hm-foot-title ">
                            <div class="logo">
                                <a href="/">
                                    <img src="/template/img/logo/logo.png" alt="logo" width="100"/>
                                </a>
                            </div>
                        </div>
                        <div class="hm-foot-para">
                            <p>
                                Імперія будівництва - одне з найкращих будівельних підприємств. Займаємося архітектурою, ремонтом та будівництвом.
                            </p>
                        </div>
                    </div>
                </div>
                <div class=" col-md-2 col-sm-6 col-xs-12">
                    <div class="hm-footer-widget">
                        <div class="hm-foot-title">
                            <h4>Основні посилання</h4>
                        </div>
                        <div class="footer-menu ">
                            <ul class="">
                                <li><a href="/" >Головна</a></li>
                                <li><a href="/about/">Про нас</a></li>
                                <li><a href="/contacts/">Контакти</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class=" col-md-3 col-sm-6 col-xs-12">
                    <div class="hm-footer-widget">
                        <div class="hm-foot-title">
                            <h4>Допоміжні посилання</h4>
                        </div>
                        <div class="footer-menu">
                            <ul class="">
                                <li><a href="/projects/">Реалізовані проєкти</a></li>
                                <li><a href="/repair/">Ремонт</a></li>
                                <li><a href="/building/">Будівництво</a></li>
                                <li><a href="/architecture/">Архітектура</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<footer class="footer-copyright">
    <div class="container">
        <div class="row">
            <div class="col-sm-7">
                <div class="foot-copyright pull-left">
                    <p>
                        &copy; Всі права захищені. Сайт розроблений Мачушник Оленою
                    </p>
                </div>
            </div>
        </div>
        <div id="scroll-Top">
            <i class="fa fa-angle-double-up return-to-top" id="scroll-top" data-toggle="tooltip" data-placement="top" title="" data-original-title="Back to Top" aria-hidden="true"></i>
        </div>
    </div>
</footer>

<script src="/template/js/jquery.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
<script type="text/javascript" src="/template/js/bootstrap.min.js"></script>
<script src="/template/js/bootsnav.js"></script>
<script src="/template/js/jquery.hc-sticky.min.js" type="text/javascript"></script>
<script src="/template/js/jquery.magnific-popup.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
<script type="text/javascript" src="/template/js/owl.carousel.min.js"></script>
<script src="/template/js/jquery.counterup.min.js"></script>
<script src="/template/js/waypoints.min.js"></script>
<script type="text/javascript" src="/template/js/jak-menusearch.js"></script>
<script type="text/javascript" src="/template/js/custom.js"></script>


