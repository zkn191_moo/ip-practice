<section id="menu">
    <div class="container">
        <div class="menubar">
            <nav class="navbar navbar-default">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Навігація</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/admin/">
                        <img src="/template/img/logo/logo.png" alt="logo" width="60">
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        <li class="active"><a href="/admin/">Панель адміністратора</a></li>
                        <li class="active"><a href="/">Повернутись на основну версію сайту</a></li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>
</section>