<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>Імперія будівництва - адмінпанель</title>
    <link rel="shortcut icon" type="image/icon" href="/template/img/logo/favicon.ico"/>

    <link rel="stylesheet" href="/template/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdn.linearicons.com/free/1.0.0/icon-font.min.css">
    <link rel="stylesheet" href="/template/css/animate.css">
    <link rel="stylesheet" href="/template/css/hover-min.css">
    <link rel="stylesheet" href="/template/css/magnific-popup.css">
    <link rel="stylesheet" href="/template/css/owl.carousel.min.css">
    <link href="/template/css/owl.theme.default.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="/template/css/bootstrap.min.css">
    <link href="/template/css/bootsnav.css" rel="stylesheet"/>
    <link rel="stylesheet" href="/template/css/style.css">
    <link rel="stylesheet" href="/template/css/responsive.css">


    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

<!--[if lte IE 9]>
<p class="browserupgrade">Ви використовуєте <strong>застарілий</strong> браузер. Будь ласка <a href="https://browsehappy.com/">оновіть Ваш браузер</a> щоб покращити Ваші враження та безпеку.</p>
<![endif]-->

    <?php include 'views/layouts/header_admin.php'; ?>

<main>
    <section class="breadcrumb-area" data-background="template/img/bg/page-title.png">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="breadcrumb-content" style="flex-direction: column;">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb breadcrumb-custom p-0 m-0">
                                <li class="breadcrumb-item"><a href="/admin/" class="breadcrumb-item-color-black big-custom-text">Адмінпанель</a></li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="container">
            <div class="row">
                <div class="col-xl-10">
                    <div class="product-content">
                        <form method="post">
                            <div class="table-responsive">
                                <table class="table table-2">

                                    <tbody>

                                    <tr>

                                        <td>
                                            <div class="table-data table-data-item">
                                                <h6 class="highlighted-custom-text">Управління реалізованими проєктами</h6>
                                            </div>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            <div class="table-data table-data-item">
                                                <h6><a href="/admin/projects/" class="title">Управління реалізованими проєктами</a></h6>
                                            </div>
                                        </td>

                                        <td>
                                            <div class="table-data table-data-item">
                                                <a href="/admin/projects/" class="red-color-hover">Перейти</a>
                                            </div>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            <div class="table-data table-data-item">
                                                <h6 class="highlighted-custom-text">Управління відгуками</h6>
                                            </div>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            <div class="table-data table-data-item">
                                                <h6><a href="/admin/testimonials/" class="title">Управління відгуками</a></h6>
                                            </div>
                                        </td>

                                        <td>
                                            <div class="table-data table-data-item">
                                                <a href="/admin/testimonials/" class="red-color-hover">Перейти</a>
                                            </div>
                                        </td>

                                    </tr>

                                    <tr>
                                        <td>
                                            <div class="table-data table-data-item">
                                                <h6 class="highlighted-custom-text">Управління замовленнями</h6>
                                            </div>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            <div class="table-data table-data-item">
                                                <h6><a href="/admin/orders_architecture/" class="title">Управління замовленнями архтектури</a></h6>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="table-data table-data-item">
                                                <a href="/admin/orders_architecture/" class="red-color-hover">Перейти</a>
                                            </div>
                                        </td>
                                    </tr>


                                    <tr>
                                        <td>
                                            <div class="table-data table-data-item">
                                                <h6><a href="/admin/orders_building/" class="title">Управління замовленнями будівництва</a></h6>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="table-data table-data-item">
                                                <a href="/admin/orders_building/" class="red-color-hover">Перейти</a>
                                            </div>
                                        </td>
                                    </tr>


                                    <tr>
                                        <td>
                                            <div class="table-data table-data-item">
                                                <h6><a href="/admin/orders_repair/" class="title">Управління замовленнями ремонту</a></h6>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="table-data table-data-item">
                                                <a href="/admin/orders_repair/" class="red-color-hover">Перейти</a>
                                            </div>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            <div class="table-data table-data-item">
                                                <h6 class="highlighted-custom-text">Управління послугами</h6>
                                            </div>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            <div class="table-data table-data-item">
                                                <h6><a href="/admin/service_architecture/" class="title">Управління послугами архітектури</a></h6>
                                            </div>
                                        </td>

                                        <td>
                                            <div class="table-data table-data-item">
                                                <a href="/admin/service_architecture/" class="red-color-hover">Перейти</a>
                                            </div>
                                        </td>

                                    </tr>

                                    <tr>


                                        <td>
                                            <div class="table-data table-data-item">
                                                <h6><a href="/admin/service_building/" class="title">Управління послугами будівництва</a></h6>
                                            </div>
                                        </td>

                                        <td>
                                            <div class="table-data table-data-item">
                                                <a href="/admin/service_building/" class="red-color-hover">Перейти</a>
                                            </div>
                                        </td>

                                    </tr>

                                    <tr>


                                        <td>
                                            <div class="table-data table-data-item">
                                                <h6><a href="/admin/service_repair/" class="title">Управління послугами ремонту</a></h6>
                                            </div>
                                        </td>

                                        <td>
                                            <div class="table-data table-data-item">
                                                <a href="/admin/service_repair/" class="red-color-hover">Перейти</a>
                                            </div>
                                        </td>

                                    </tr>


                                    </tbody>
                                </table>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </section>
</main>




    <?php include 'views/layouts/footer_admin.php'; ?>
</body>
</html>