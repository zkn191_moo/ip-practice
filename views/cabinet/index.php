<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>Імперія будівництва - кабінет користувача</title>
    <link rel="shortcut icon" type="image/icon" href="/template/img/logo/favicon.ico"/>

    <link rel="stylesheet" href="/template/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdn.linearicons.com/free/1.0.0/icon-font.min.css">
    <link rel="stylesheet" href="/template/css/animate.css">
    <link rel="stylesheet" href="/template/css/hover-min.css">
    <link rel="stylesheet" href="/template/css/magnific-popup.css">
    <link rel="stylesheet" href="/template/css/owl.carousel.min.css">
    <link href="/template/css/owl.theme.default.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="/template/css/bootstrap.min.css">
    <link href="/template/css/bootsnav.css" rel="stylesheet"/>
    <link rel="stylesheet" href="/template/css/style.css">
    <link rel="stylesheet" href="/template/css/responsive.css">


    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

<!--[if lte IE 9]>
<p class="browserupgrade">Ви використовуєте <strong>застарілий</strong> браузер. Будь ласка <a href="https://browsehappy.com/">оновіть Ваш браузер</a> щоб покращити Ваші враження та безпеку.</p>
<![endif]-->

    <?php include 'views/layouts/header.php'; ?>
    <main>
        <section class="breadcrumb-area" data-background="template/img/bg/page-title.png">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="breadcrumb-content" style="flex-direction: column;">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb breadcrumb-custom p-0 m-0">
                                    <li class="breadcrumb-item"><a href="/" class="breadcrumb-item-color-black">Головна</a></li>
                                    //
                                    <li class="breadcrumb-item active breadcrumb-item-color-black" aria-current="page">Кабінет користувача</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="service mt-100 mb-100">
            <div class="container container-1430">
                    <h3>Привіт, <?php echo $user['name']; ?>!</h3>
                    <div class="row service-row">
                        <div class="service-box service-box-2">
                        <div class="col-md-4">
                            <div class="service-box service-box-2">

                                <div class="service-content text-center">
                                    <h6 class="title"><a href="/cabinet/edit" class="link-item-color-black">Редагувати власні дані</a></h6>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="service-box service-box-2">

                                <div class="service-content text-center">
                                    <h6 class="title"><a href="/admin/" class="link-item-color-black">Перейти в панель адміністратора</a></h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
    <?php include 'views/layouts/footer.php'; ?>
</body>
</html>