<!doctype html>
<html class="no-js" lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,400i,700,700i,900,900i" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700,900" rel="stylesheet">


    <title>Імперія будівництва - про нас</title>


    <link rel="shortcut icon" type="image/icon" href="/template/img/logo/favicon.ico"/>
    <link rel="stylesheet" href="/template/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdn.linearicons.com/free/1.0.0/icon-font.min.css">
    <link rel="stylesheet" href="/template/css/animate.css">
    <link rel="stylesheet" href="/template/css/hover-min.css">
    <link rel="stylesheet" href="/template/css/magnific-popup.css">
    <link rel="stylesheet" href="/template/css/owl.carousel.min.css">
    <link href="/template/css/owl.theme.default.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="/template/css/bootstrap.min.css">
    <link href="/template/css/bootsnav.css" rel="stylesheet"/>
    <link rel="stylesheet" href="/template/css/style.css">
    <link rel="stylesheet" href="/template/css/responsive.css">

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
<!--[if lte IE 9]>
<p class="browserupgrade">Ви використовуєте <strong>застарілий</strong> браузер. Будь ласка <a href="https://browsehappy.com/">оновіть Ваш браузер</a> щоб покращити Ваші враження та безпеку.</p>
<![endif]-->


<?php require_once('views/layouts/header.php'); ?>

<section class="about-part">
    <div class="container">
        <div class="about-part-details text-center">
            <h2>про нас</h2>
            <div class="about-part-content">
                <div class="breadcrumbs">
                    <div class="container">
                        <ol class="breadcrumb">
                            <li><a href="/">головна</a><span>//</span></li>
                            <li><a href="/about/">про нас</a></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="about-history">
    <div class="container">
        <div class="about-history-content">
            <div class="row">
                <div class="col-md-offset-1 col-md-6 col-sm-12 center-display-block">
                    <div class="single-about-history">
                        <div class="about-history-txt">
                            <h2>Наша історія заснування</h2>
                            <p>
                                Приватне підприємство "Імперія будівництва" була заснована 26 серпня 2016 року Загладьком Віктором Миколайовичем та Кратюк Людмилою Андріївною.
                            </p>

                            <div class="main-timeline">
                                <div class="row">
                                    <div class="col-md-2 col-sm-2">
                                        <div class="experience-time">
                                            <h3>2016</h3>
                                        </div>
                                    </div>
                                    <div class="col-md-10 col-sm-10">
                                        <div class="timeline">
                                            <div class="timeline-content">
                                                <h4 class="title"> <span><i class="fa fa-circle-o" aria-hidden="true"></i></span></h4>
                                                <ul class="description">
                                                    <li>Рік заснування підприємства. Успішна робота із Житомирським музичним фаховим коледжем ім. В.С.Косенка.</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-2 col-sm-2">
                                        <div class="experience-time">
                                            <h3>2017</h3>
                                        </div>
                                    </div>
                                    <div class="col-md-10 col-sm-10">
                                        <div class="timeline">
                                            <div class="timeline-content">
                                                <h4 class="title"> <span><i class="fa fa-circle-o" aria-hidden="true"></i></span></h4>
                                                <ul class="description">
                                                    <li>Успішна робота із укрпоштою, Головенківською сільською радою, відділом культури, національностей, релігій, молоді та спорту Іванківської районної державної адміністрації Київської області, Глибочицькою сільською радою Житомирської області, департаментом містобудування та земельних відносин Житомирської міської ради.</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-2 col-sm-2">
                                        <div class="experience-time">
                                            <h3>2018</h3>
                                        </div>
                                    </div>
                                    <div class="col-md-10 col-sm-10">
                                        <div class="timeline">

                                            <div class="timeline-content">
                                                <h4 class="title"> <span><i class="fa fa-circle-o" aria-hidden="true"></i></span></h4>

                                                <ul class="description">
                                                    <li>Успішна робота із Глибочицькою сільською радою Житомирської області, відділом освіти Радомишльської міської ради та Чуднівської районної державної адміністрації Житомирської області, Краснопільською сільсько радою, комунальним підприємством "Центр інвестицій" Житомирської міської ради, Житомирським музичним фаховим коледжем ім. В.С.Косенка, Станишівською сільською радою, департаментом регіонального розвитку Житомирської обласної державної адміністрації, відділом освіти Житомирської райдержадміністрації.</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-2 col-sm-2">
                                        <div class="experience-time">
                                            <h3>2019</h3>
                                        </div>
                                    </div>
                                    <div class="col-md-10 col-sm-10">
                                        <div class="timeline">
                                            <div class="timeline-content">
                                                <h4 class="title"> <span><i class="fa fa-circle-o" aria-hidden="true"></i></span></h4>
                                                <ul class="description">
                                                    <li>Успішна робота із Станишівською сільською радою, Оліївською сільською радою Житомирського району, Левківською сільською радою, Глибочицькою сільською радою Житомирської області, Тетерівською сільською радою Житомирської області, Житомирським обласним ліцеєм Житомирської обласної ради.</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-2 col-sm-2">
                                        <div class="experience-time">
                                            <h3>2020</h3>
                                        </div>
                                    </div>
                                    <div class="col-md-10 col-sm-10">
                                        <div class="timeline">
                                            <div class="timeline-content">
                                                <h4 class="title"> <span><i class="fa fa-circle-o" aria-hidden="true"></i></span></h4>
                                                <ul class="description">
                                                    <li>Успішна робота із Станишівською сільською радою, Левківською сільською радою, Тетерівською сільською радою Житомирського району, Оліївською сільською радою Житомирського району, комунальним підприємством по експлуатації адмінбудинків Житомирської обласної ради, відділом освіти Житомирської райдержадміністрації.</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-2 col-sm-2">
                                        <div class="experience-time">
                                            <h3>2021</h3>
                                        </div>
                                    </div>
                                    <div class="col-md-10 col-sm-10">
                                        <div class="timeline">
                                            <div class="timeline-content">
                                                <h4 class="title"> <span><i class="fa fa-circle-o" aria-hidden="true"></i></span></h4>
                                                <ul class="description">
                                                    <li>Успішна робота із Станишівською сільською радою, відділом освіти, культури, молоді та спорту Глибочицької сільської ради Житомирського району, КЗ Житомирським обласним інститутом післядипломної педагогічної освіти Житомирської обласної ради, Оліївською сільською радою Житомирського району, Новогуйвинською селищною радою Житомирського району, Житомирським агротехнічним фаховим коледжем.</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-2 col-sm-2">
                                        <div class="experience-time">
                                            <h3>зараз</h3>
                                        </div>
                                    </div>
                                    <div class="col-md-10 col-sm-10">
                                        <div class="timeline">
                                            <div class="timeline-content">
                                                <h4 class="title"> <span><i class="fa fa-circle-o" aria-hidden="true"></i></span></h4>
                                                <ul class="description">
                                                    <li>З нетерпінням чекаємо на нові успішні замовлення.</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section  class="statistics">
        <div class="container">
            <div class="statistics-counter ">
            </div>
        </div>
    </section>

    <div class="about-history">
        <div class="container">
            <div class="about-history-content">
                <div class="row">
                    <div class="about-vission-content">
                        <div class="col-md-6 col-sm-12">
                            <div class="single-about-history">
                                <div class="about-history-txt">
                                    <h2>Наші види діяльності</h2>
                                    <div class="main-timeline  xtra-timeline">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="timeline timeline-ml-20">
                                                    <div class="timeline-content">
                                                        <h4 class="title"> <span><i class="fa fa-circle-o" aria-hidden="true"></i></span></h4>
                                                        <ul class="description">
                                                            <li>41.20 Будівництво житлових і нежитлових будівель</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="timeline timeline-ml-20">
                                                    <div class="timeline-content">
                                                        <h4 class="title"> <span><i class="fa fa-circle-o" aria-hidden="true"></i></span></h4>
                                                        <ul class="description">
                                                            <li>43.11 Знесення</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="timeline timeline-ml-20">
                                                    <div class="timeline-content">
                                                        <h4 class="title"> <span><i class="fa fa-circle-o" aria-hidden="true"></i></span></h4>
                                                        <ul class="description">
                                                            <li>43.12 Підготовчі роботи на будівельному майданчику</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="timeline timeline-ml-20">
                                                    <div class="timeline-content">
                                                        <h4 class="title"> <span><i class="fa fa-circle-o" aria-hidden="true"></i></span></h4>
                                                        <ul class="description">
                                                            <li>43.13 Розвідувальне буріння</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="timeline timeline-ml-20">
                                                    <div class="timeline-content">
                                                        <h4 class="title"> <span><i class="fa fa-circle-o" aria-hidden="true"></i></span></h4>
                                                        <ul class="description">
                                                            <li>43.21 Електромонтажні роботи</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="timeline timeline-ml-20">
                                                    <div class="timeline-content">
                                                        <h4 class="title"> <span><i class="fa fa-circle-o" aria-hidden="true"></i></span></h4>
                                                        <ul class="description">
                                                            <li>43.22 Монтаж водопровідних мереж, систем опалення та кондиціонування</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="timeline timeline-ml-20">
                                                    <div class="timeline-content">
                                                        <h4 class="title"> <span><i class="fa fa-circle-o" aria-hidden="true"></i></span></h4>
                                                        <ul class="description">
                                                            <li>43.29 Інші будівельно-монтажні роботи</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="timeline timeline-ml-20">
                                                    <div class="timeline-content">
                                                        <h4 class="title"> <span><i class="fa fa-circle-o" aria-hidden="true"></i></span></h4>
                                                        <ul class="description">
                                                            <li>43.31 Штукатурні роботи</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="timeline timeline-ml-20">
                                                    <div class="timeline-content">
                                                        <h4 class="title"> <span><i class="fa fa-circle-o" aria-hidden="true"></i></span></h4>
                                                        <ul class="description">
                                                            <li>43.33 Покриття підлоги й облицювання стін</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="timeline timeline-ml-20">
                                                    <div class="timeline-content">
                                                        <h4 class="title"> <span><i class="fa fa-circle-o" aria-hidden="true"></i></span></h4>
                                                        <ul class="description">
                                                            <li>43.34 Малярні роботи та скління</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="timeline timeline-ml-20">
                                                    <div class="timeline-content">
                                                        <h4 class="title"> <span><i class="fa fa-circle-o" aria-hidden="true"></i></span></h4>
                                                        <ul class="description">
                                                            <li>43.39 Інші роботи із завершення будівництва</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="timeline timeline-ml-20">
                                                    <div class="timeline-content">
                                                        <h4 class="title"> <span><i class="fa fa-circle-o" aria-hidden="true"></i></span></h4>
                                                        <ul class="description">
                                                            <li>43.91 Покрівельні роботи</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="timeline timeline-ml-20">
                                                    <div class="timeline-content">
                                                        <h4 class="title"> <span><i class="fa fa-circle-o" aria-hidden="true"></i></span></h4>
                                                        <ul class="description">
                                                            <li>43.99 Інші спеціалізовані будівельні роботи, н.в.і.у.</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="timeline timeline-ml-20">
                                                    <div class="timeline-content">
                                                        <h4 class="title"> <span><i class="fa fa-circle-o" aria-hidden="true"></i></span></h4>
                                                        <ul class="description">
                                                            <li>71.11 Діяльність у сфері архітектури</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="timeline timeline-ml-20">
                                                    <div class="timeline-content">
                                                        <h4 class="title"> <span><i class="fa fa-circle-o" aria-hidden="true"></i></span></h4>
                                                        <ul class="description">
                                                            <li>71.12 Діяльність у сфері інжинірингу, геології та геодезії, надання послуг технічного консультування в цих сферах</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="timeline timeline-ml-20">
                                                    <div class="timeline-content">
                                                        <h4 class="title"> <span><i class="fa fa-circle-o" aria-hidden="true"></i></span></h4>
                                                        <ul class="description">
                                                            <li>41.10 Організація будівництва будівель</li>

                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-offset-1 col-md-5 col-sm-12">
                            <div class="single-about-history">
                                <div class="about-history-img">
                                    <img src="/template/img/about/about_working.jpg" alt="робочий процес">
                                </div>
                                <div class="about-history-img about-history-img-second">
                                    <img src="/template/img/about/about_building.jpg" alt="робочий процес">
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section  class="we-do">
        <div class="container">
            <div class="we-do-details">
                <div class="section-header text-center">
                    <h2>Наша виконана робота</h2>
                </div>
                <div class="we-do-carousel">
                    <div class="row">
                        <div class="col-sm-4 col-xs-12">
                            <div class="single-we-do-box text-center">
                                <div class="we-do-description">
                                    <div class="we-do-info">
                                        <div class="we-do-img">
                                            <img src="/template/img/home/consultency.png" alt="image of consultency" />
                                        </div>
                                        <div class="we-do-topics">
                                            <h2>
                                                Професійна
                                            </h2>
                                        </div>
                                    </div>
                                    <div class="we-do-comment">
                                        <p>
                                            Наші робітники - профксіонали своєї справи. Завжди виконають роботу на найвищому рівні.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4 col-xs-12">
                            <div class="single-we-do-box text-center">
                                <div class="we-do-description">
                                    <div class="we-do-info">
                                        <div class="we-do-img">
                                            <img src="/template/img/home/busisness_grow.png" alt="image of business" />
                                        </div>
                                        <div class="we-do-topics">
                                            <h2>
                                                Одна з найкращих
                                            </h2>
                                        </div>
                                    </div>
                                    <div class="we-do-comment">
                                        <p>
                                            Якість виконаної роботи завжди чудова.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4 col-xs-12">
                            <div class="single-we-do-box text-center">
                                <div class="we-do-description">
                                    <div class="we-do-info">
                                        <div class="we-do-img">
                                            <img src="/template/img/home/support-logo.png" alt="image of support" />
                                        </div>
                                        <div class="we-do-topics">
                                            <h2>
                                                Із підтримкою замовника
                                            </h2>
                                        </div>
                                    </div>
                                    <div class="we-do-comment">
                                        <p>
                                            Наше підприємство зажди допомагає і радить замовнику при виникненні будь-яких запитань.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<?php require_once('views/layouts/footer.php'); ?>
</body>
</html>