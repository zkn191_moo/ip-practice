<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,400i,700,700i,900,900i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700,900" rel="stylesheet">

    <title>Імперія будівництва - реалізовані проєкти</title>
    <link rel="shortcut icon" type="image/icon" href="/template/img/logo/favicon.ico"/>

    <link rel="stylesheet" href="/template/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdn.linearicons.com/free/1.0.0/icon-font.min.css">
    <link rel="stylesheet" href="/template/css/animate.css">
    <link rel="stylesheet" href="/template/css/hover-min.css">
    <link rel="stylesheet" href="/template/css/owl.carousel.min.css">
    <link href="assets/css/owl.theme.default.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="/template/css/magnific-popup.css">
    <link rel="stylesheet" href="/template/css/bootstrap.min.css">
    <link href="/template/css/bootsnav.css" rel="stylesheet"/>
    <link rel="stylesheet" href="/template/css/style.css">
    <link rel="stylesheet" href="/template/css/responsive.css">

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
<!--[if lte IE 9]>
<p class="browserupgrade">Ви використовуєте <strong>застарілий</strong> браузер. Будь ласка <a href="https://browsehappy.com/">оновіть Ваш браузер</a> щоб покращити Ваші враження та безпеку.</p>
<![endif]-->

<?php require_once('views/layouts/header.php'); ?>

<section class="about-part team-part">
    <div class="container">
        <div class="about-part-details text-center">
            <h2>реалізовані проєкти</h2>
            <div class="about-part-content">
                <div class="breadcrumbs">
                    <div class="container">
                        <ol class="breadcrumb">
                            <li><a href="/">головна</a><span>//</span></li>
                            <li><a href="/projects/">реалізовані проєкти</a></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="team" class="team  team-main">
    <div class="container">
        <div class="team-details">
            <div class="project-header team-header team-head text-center">
                <h2>Наші реалізовані проєкти</h2>
                <p>
                    Детальний перелік реалізованих проєктів, які виконало приватне підприємство "Імперія будівництва"
                </p>
            </div>
            <div class="team-card">
                <div class="container">
                    <div class="row">
                        <table class="table table-striped table-my-custom">
                            <thead>
                            <tr>
                                <th scope="col">Дата</th>
                                <th scope="col">Опис</th>
                                <th scope="col">Замовник</th>
                                <th scope="col">Сума, грн</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach($finishedProjectsList as $project):?>
                                <tr>
                                    <td><?php echo($project['contract_date']); ?></td>
                                    <td><?php echo($project['name']); ?></td>
                                    <td><?php echo($project['client']); ?></td>
                                    <td><?php echo($project['money_amount']); ?></td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php require_once('views/layouts/footer.php'); ?>

</body>

</html>