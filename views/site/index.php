<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,400i,700,700i,900,900i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700,900" rel="stylesheet">

    <title>Імперія будівництва</title>
    <link rel="shortcut icon" type="image/icon" href="/template/img/logo/favicon.ico"/>

    <link rel="stylesheet" href="/template/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdn.linearicons.com/free/1.0.0/icon-font.min.css">
    <link rel="stylesheet" href="/template/css/animate.css">
    <link rel="stylesheet" href="/template/css/hover-min.css">
    <link rel="stylesheet" href="/template/css/magnific-popup.css">
    <link rel="stylesheet" href="/template/css/owl.carousel.min.css">
    <link href="/template/css/owl.theme.default.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="/template/css/bootstrap.min.css">
    <link href="/template/css/bootsnav.css" rel="stylesheet"/>
    <link rel="stylesheet" href="/template/css/style.css">
    <link rel="stylesheet" href="/template/css/responsive.css">


    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
<!--[if lte IE 9]>
<p class="browserupgrade">Ви використовуєте <strong>застарілий</strong> браузер. Будь ласка <a href="https://browsehappy.com/">оновіть Ваш браузер</a> щоб покращити Ваші враження та безпеку.</p>
<![endif]-->

<?php require_once('views/layouts/header.php'); ?>

<section class="header-slider-area">
    <div id="carousel-example-generic" class="carousel slide carousel-fade" data-ride="carousel">

        <ol class="carousel-indicators">
            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
        </ol>

        <div class="carousel-inner" role="listbox">
            <div class="item active">
                <div class="single-slide-item slide-1">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="single-slide-item-content">
                                    <h2>Вас вітає <br> Імперія Будівництва</h2>
                                    <p>
                                        Одне з найкращих будівельних підприємств. Займаємося архітектурою, ремонтом та будівництвом.
                                    </p>
                                    <button type="button" class="slide-btn">
                                        <a href="/projects/">проєкти</a>
                                    </button>
                                    <button type="button"  class="slide-btn">
                                        <a href="/about/">про нас</a>
                                    </button>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="single-slide-item slide-2">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="single-slide-item-content">
                                    <h2>Вас вітає <br> Імперія Будівництва</h2>
                                    <p>
                                        Одне з найкращих будівельних підприємств. Займаємося архітектурою, ремонтом та будівництвом.
                                    </p>
                                    <button type="button"  class="slide-btn">
                                        <a href="/projects/">проєкти</a>
                                    </button>
                                    <button type="button"  class="slide-btn">
                                        <a href="/about/">про нас</a>
                                    </button>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
            <span class="lnr lnr-chevron-left"></span>
        </a>
        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
            <span class="lnr lnr-chevron-right"></span>
        </a>
    </div>

</section>

<section  class="we-do we-do-margin-top">
    <div class="container">
        <div class="we-do-details">
            <div class="section-header text-center">
                <h2>чим ми займаємось</h2>
                <p>
                    Основа нашого бізнесу - це продуктивна заємодія з клієнтами, що базуються на довірі та взаємоповазі.
                    Кожен з наших клієнтів може розраховувати на наш час і увагу та цілковито покластися на компетентність та надійність нашої роботи.
                </p>
            </div>
            <div class="we-do-carousel">
                <div class="row">
                    <div class="col-sm-4 col-xs-12">
                        <div class="single-we-do-box text-center">
                            <div class="we-do-description">
                                <div class="we-do-info">
                                    <div class="we-do-img">
                                        <img src="/template/img/home/building.png" alt="Будівництво" />
                                    </div>
                                    <div class="we-do-topics">
                                        <h2>
                                            <a href="/building/">
                                                Будівництво
                                            </a>
                                        </h2>
                                    </div>
                                </div>
                                <div class="we-do-comment">
                                    <p>
                                        Будуємо з нуля житлові та нежитлові будівлі. Робимо підготовчі роботи на будівельному майданчику.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 col-xs-12">
                        <div class="single-we-do-box text-center">
                            <div class="we-do-description">
                                <div class="we-do-info">
                                    <div class="we-do-img">
                                        <img src="/template/img/home/repair.png" alt="Ремонт" />
                                    </div>
                                    <div class="we-do-topics">
                                        <h2>
                                            <a href="/repair/">
                                                Ремонт
                                            </a>
                                        </h2>
                                    </div>
                                </div>
                                <div class="we-do-comment">
                                    <p>
                                        Виконуємо штукатурні роботи, покриття підлоги й облицювання стін, малярні роботи та скління, покрівельні роботи.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 col-xs-12">
                        <div class="single-we-do-box text-center">
                            <div class="we-do-description">
                                <div class="we-do-info">
                                    <div class="we-do-img">
                                        <img src="/template/img/home/architecture.png" alt="Архітектура" />
                                    </div>
                                    <div class="we-do-topics">
                                        <h2>
                                            <a href="/architecture/">
                                                Архітектура
                                            </a>

                                        </h2>
                                    </div>
                                </div>
                                <div class="we-do-comment">
                                    <p>
                                        Займаємося діяльністю у сфері архітектури, проєктування, інжинірингу.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="about-us">
    <div class="container">
        <div class="about-us-content">
            <div class="row">
                <div class="col-sm-6">
                    <div class="single-about-us">
                        <div class="about-us-txt">
                            <h2>Про нас</h2>
                            <p>
                                Імперія будівництва – це стабільне підприємство, яке динамічно розвивається та забезпечує надання високої якості послуг в будівельній галузі. Починаючи із 2016 року підприємство є учасником у 140 закупівлях та переможцем у 133 закупівлях на суму 7.12 млн грн.
                            </p>
                            <div class="project-btn">
                                <a href="/about/"  class="project-view">дізнатися більше
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="single-about-us">
                        <div class="about-us-img">
                            <img src="/template/img/about/about-part.jpg" alt="Про нас">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>

<section  class="service">
    <div class="container">
        <div class="service-details">
            <div class="section-header text-center">
                <h2>що ми пропонуємо</h2>
                <p>
                    Будівельне підприємство "Імперія будівництва" здійснює будівництво об’єктів будь-якого рівня складності та на різних етапах будівництва.
                </p>
            </div>
            <div class="service-content-one">
                <div class="row">
                    <div class="col-sm-4 col-xs-12">
                        <div class="service-single text-center">
                            <div class="service-img">
                                <img src="/template/img/service/service1.png" alt="image of service" />
                            </div>
                            <div class="service-txt">
                                <h2>
                                    професійне планування
                                </h2>
                                <p>
                                    Якісно виконуємо архітектурне планування і планування будівництва.
                                </p>

                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 col-xs-12">
                        <div class="service-single text-center">
                            <div class="service-img">
                                <img src="/template/img/service/service2.png" alt="image of service" />
                            </div>
                            <div class="service-txt">
                                <h2>
                                    злагодженість роботи
                                </h2>
                                <p>
                                    Уся робота виконується злагоджено за заздалегідь створеним планом.
                                </p>

                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 col-xs-12">
                        <div class="service-single text-center">
                            <div class="service-img">
                                <img src="/template/img/service/service3.png" alt="image of service" />
                            </div>
                            <div class="service-txt">
                                <h2>
                                    економія фінансів
                                </h2>
                                <p>
                                    Ми закуповуємо будівельні матеріали за вигідним співвідношенням ціна/якість
                                </p>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="service-content-two">
                <div class="row">
                    <div class="col-sm-4 col-xs-12">
                        <div class="service-single text-center">
                            <div class="service-img">
                                <img src="/template/img/service/service4.png" alt="image of service" />
                            </div><!--/.service-img-->
                            <div class="service-txt">
                                <h2>
                                    Ліквідація ризиків
                                </h2>
                                <p>
                                    Виконуємо злагоджений менеджмент щодо неможливості з'явлення ризиків у майбутньому.
                                </p>

                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 col-xs-12">
                        <div class="service-single text-center">
                            <div class="service-img">
                                <img src="/template/img/service/service5.png" alt="image of service" />
                            </div>
                            <div class="service-txt">
                                <h2>
                                    Робота експертів
                                </h2>
                                <p>
                                    На підприємстві "Імперія будівництва" працюють виключно експерти у своїй галузі.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 col-xs-12">
                        <div class="service-single text-center">
                            <div class="service-img">
                                <img src="/template/img/service/service6.png" alt="image of service" />
                            </div>
                            <div class="service-txt">
                                <h2>
                                    Підтримка клієнтів 24/7
                                </h2>
                                <p>
                                    Завжди допомагаємо клієнтам, якщо у них з'явились будь-які питання.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>

<section  class="statistics">
    <div class="container">
        <div class="statistics-counter ">
            <div class="col-md-3 col-sm-6">
                <div class="single-ststistics-box">
                    <div class="statistics-img">
                        <img src="/template/img/counter/counter1.png" alt="counter-icon" />
                    </div>
                    <div class="statistics-content">
                        <div class="counter">2000</div>
                        <h3>робочих днів</h3>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="single-ststistics-box">
                    <div class="statistics-img">
                        <img src="/template/img/counter/counter2.png" alt="counter-icon" />
                    </div>
                    <div class="statistics-content">
                        <div class="counter">130</div>
                        <h3>виконаних проєктів</h3>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="single-ststistics-box">
                    <div class="statistics-img">
                        <img src="/template/img/counter/counter3.png" alt="counter-icon" />
                    </div>
                    <div class="statistics-content">
                        <div class="counter">300</div>
                        <h3>підтримкок користувачам</h3>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="single-ststistics-box">
                    <div class="statistics-img">
                        <img src="/template/img/counter/counter4.png" alt="counter-icon" />
                    </div>
                    <div class="statistics-content">
                        <div class="counter">200</div>
                        <h3>задоволених замовників</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="project"  class="project">
    <div class="container">
        <div class="project-details">
            <div class="project-header text-left">
                <h2>Наші Виконані Проєкти</h2>
                <p>
                    Наші завершені проєкти вигідні у ціні й найкращі по якості.
                </p>
            </div>
            <div class="project-content">
                <div class="gallery-content">
                    <div class="isotope">
                        <div class="row">
                            <div class="col-md-18 col-sm-12">
                                <div class="row">
                                    <table class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th scope="col">Дата</th>
                                            <th scope="col">Опис</th>
                                            <th scope="col">Замовник</th>
                                            <th scope="col">Сума, грн</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php $j = 0; ?>
                                        <?php foreach($finishedProjectsList as $project):?>
                                            <?php $j++; ?>
                                            <?php if($j == 10) {break;} ?>
                                            <tr>
                                                <td><?php echo($project['contract_date']); ?></td>
                                                <td><?php echo($project['name']); ?></td>
                                                <td><?php echo($project['client']); ?></td>
                                                <td><?php echo($project['money_amount']); ?></td>
                                            </tr>
                                        <?php endforeach; ?>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="project-btn text-center">
            <a href="/projects/"  class="project-view">показати всі</a>
        </div>
    </div>
</section>

<section id="pricing" class="pricing">
    <div class="container">
        <div class="pricing-details">
            <div class="section-header text-center">
                <h2 class="price-head">наша таблиця цін</h2>
                <p>
                    Тут можна побачити ціни за послуги, які найчастіше замовляють
                </p>
            </div>
            <div class="pricing-content">
                <div class="row">
                    <?php $i = 0; ?>
                    <?php foreach($allAdvertisingServices as $work): ?>
                    <?php $i++; ?>
                    <?php if($i == 5) {break;} ?>
                        <div class="col-md-3 col-sm-6">
                            <div class="pricing-box">
                                <div class="pricing-header">
                                    <h2><?php echo($work['name']); ?></h2>
                                    <h3 class="packeg_p"><?php echo($work['price']); ?> <span>грн</span></h3>
                                    <p>за <?php echo($work['work_dimension']); ?></p>
                                </div>
                                <ul class="plan-lists">
                                    <li><?php echo($work['internal_external']); ?></li>
                                </ul>

                                <div class="project-btn pricing-btn text-center">
                                    <a href="/contacts/"  class="project-view">
                                        обрати
                                    </a>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</section>

<section   class="testemonial">
    <div class="container">
        <div class="section-header text-center">
            <h2>
						<span>
							що наші клієнти говорять про нас
						</span>
            </h2>
        </div>

        <div class="owl-carousel owl-theme" id="testemonial-carousel">
            <?php if(isset($testimonials)): ?>
            <?php foreach($testimonials as $testimonial): ?>
            <div class="home1-testm item">
                <div class="home1-testm-single text-center">

                    <div class="home1-testm-txt">
								<span class="icon section-icon">
									<i class="fa fa-quote-left" aria-hidden="true"></i>
								</span>
                        <p>
                            <?php echo($testimonial['testimonial']); ?>
                        </p>
                        <h3>
                            <?php echo($testimonial['name'] . ' ' . $testimonial['surname']); ?>
                        </h3>
                    </div>
                </div>
            </div>
            <?php endforeach; ?>
            <?php endif; ?>
        </div>
    </div>
</section>

<section class="clients">
    <div class="container">
        <div class="clients-area">
            <div class="owl-carousel owl-theme" id="client">
                <div class="item">
                    <a href="#">
                        <img src="/template/img/client/client1.png" alt="brand-image" />
                    </a>
                </div>
                <div class="item">
                    <a href="#">
                        <img src="/template/img/client/client2.png" alt="brand-image" />
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>

<section  class="contact">
    <div class="container">
        <div class="contact-details">
            <div class="section-header contact-head  text-center">
                <h2>Вже стали нашим клієнтом?</h2>
                <p>
                    Дякуємо за співпрацю! Тут Ви можете залишити відгук про компанію.
                </p>
            </div>
            <div class="contact-content">
                <div class="row">
                    <div class="col-sm-15">
                        <div class="single-contact-box">
                            <div class="contact-form">
                                <h3>ННадішліть нам свій відгук про підприємство</h3>
                                <form method="post">
                                    <div class="row">
                                        <div class="col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <input type="text" class="form-control" id="userName" placeholder="Ім'я" name="userName" required>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <input type="text" class="form-control" id="userSurname" placeholder="Прізвище" name="userSurname" required>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <textarea class="form-control" rows="7" id="userTestimonial" name="userTestimonial" placeholder="Відгук" required></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="single-contact-btn pull-right">
                                                <button class="contact-btn" type="submit" name="submit">надіслати</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section  id="new-project" class="new-project">
    <div class="container">
        <div class="new-project-details">
            <div class="row">
                <div class="col-md-10 col-sm-8">
                    <div class="single-new-project">
                        <h3>
                            Хочете почати новий проєкт з нами? Почнімо!
                        </h3>
                    </div>
                </div>
                <div class="col-md-2 col-sm-4">
                    <div class="single-new-project">
                        <a href="/contacts/" class="slide-btn">
                            почати
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php require_once('views/layouts/footer.php'); ?>
</body>
</html>
