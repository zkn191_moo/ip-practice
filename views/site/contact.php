<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,400i,700,700i,900,900i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700,900" rel="stylesheet">

    <title>Імперія будівництва - контакти</title>
    <link rel="shortcut icon" type="image/icon" href="/template/img/logo/favicon.ico"/>

    <link rel="stylesheet" href="/template/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdn.linearicons.com/free/1.0.0/icon-font.min.css">
    <link rel="stylesheet" href="/template/css/animate.css">
    <link rel="stylesheet" href="/template/css/hover-min.css">
    <link rel="stylesheet" href="/template/css/magnific-popup.css">
    <link rel="stylesheet" href="/template/css/owl.carousel.min.css">
    <link href="/template/css/owl.theme.default.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="/template/css/bootstrap.min.css">
    <link href="/template/css/bootsnav.css" rel="stylesheet"/>
    <link rel="stylesheet" href="/template/css/style.css">
    <link rel="stylesheet" href="/template/css/responsive.css">

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
<!--[if lte IE 9]>
<p class="browserupgrade">Ви використовуєте <strong>застарілий</strong> браузер. Будь ласка <a href="https://browsehappy.com/">оновіть Ваш браузер</a> щоб покращити Ваші враження та безпеку.</p>
<![endif]-->



<?php require_once('views/layouts/header.php'); ?>


<section class="about-part">
    <div class="container">
        <div class="about-part-details text-center">
            <h2>контакти</h2>
            <div class="about-part-content">
                <div class="breadcrumbs">
                    <div class="container">
                        <ol class="breadcrumb">
                            <li><a href="/">головна</a><span>//</span></li>
                            <li><a href="/contacts/">контакти</a></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section  class="contact">
    <div class="container">
        <div class="contact-details">
            <div class="section-header contact-head  text-center">
                <h2>зв'язатися з нами</h2>
                <p>
                    Тут Ви можете побачити контактні дані нашого підприємства та надіслати нам своє повідомлення.
                </p>
            </div>
            <div class="contact-content">
                <div class="row">
                    <div class="col-sm-offset-1 col-sm-5">
                        <div class="single-contact-box">
                            <div class="contact-right">
                                <div class="contact-adress">
                                    <div class="contact-office-address">
                                        <h3>Контактна інформація</h3>
                                        <p>
                                            10014, Житомирська область, місто Житомир, вулиця Київська, будинок 22.
                                        </p>
                                        <div class="contact-online-address">
                                            <div class="single-online-address">
                                                <i class="fa fa-phone"></i>
                                                0412480220
                                            </div>
                                            <div class="single-online-address">
                                                <i class="fa fa-envelope-o"></i>
                                                <span>imperiya.bud@ukr.net</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-5">
                        <div class="single-contact-box">
                            <div class="contact-form">
                                <h3>Залиште нам своє повідомлення тут</h3>
                                <form method="post">
                                    <div class="row">
                                        <div class="col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <input type="text" class="form-control" id="userSurname" placeholder="Прізвище" name="userSurname" required>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <input type="text" class="form-control" id="userName" placeholder="Ім'я" name="userName" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <input type="email" class="form-control" id="userEmail" placeholder="Email" name="userEmail" required>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <input type="number" class="form-control" id="userPhone" placeholder="Телефон" name="userPhone" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <textarea class="form-control" rows="7" id="userText" name="userText" placeholder="Текст повідомлення" ></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="single-contact-btn pull-right">
                                                <button class="contact-btn" type="submit" name="submit">надіслати</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php require_once('views/layouts/footer.php'); ?>

</body>
</html>





