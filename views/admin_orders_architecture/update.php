<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Імперія будівництва - оновлення замовлення архітектури</title>

    <link rel="shortcut icon" type="image/icon" href="/template/img/logo/favicon.ico"/>

    <link rel="stylesheet" href="/template/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdn.linearicons.com/free/1.0.0/icon-font.min.css">
    <link rel="stylesheet" href="/template/css/animate.css">
    <link rel="stylesheet" href="/template/css/hover-min.css">
    <link rel="stylesheet" href="/template/css/magnific-popup.css">
    <link rel="stylesheet" href="/template/css/owl.carousel.min.css">
    <link href="/template/css/owl.theme.default.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="/template/css/bootstrap.min.css">
    <link href="/template/css/bootsnav.css" rel="stylesheet"/>
    <link rel="stylesheet" href="/template/css/style.css">
    <link rel="stylesheet" href="/template/css/responsive.css">


    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<!--[if lte IE 9]>
<p class="browserupgrade">Ви використовуєте <strong>застарілий</strong> браузер. Будь ласка <a href="https://browsehappy.com/">оновіть Ваш браузер</a> щоб покращити Ваші враження та безпеку.</p>
<![endif]-->
<?php include 'views/layouts/header_admin.php'; ?>
<main>
    <section class="breadcrumb-area" data-background="template/img/bg/page-title.png">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="breadcrumb-content" style="flex-direction: column;">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb breadcrumb-custom p-0 m-0">
                                <li class="middle-custom-text"><a href="/admin/" class="middle-custom-text">Адмінпанель</a><span>//</span></li>
                                <li><a href="/admin/orders_architecture/" class="middle-custom-text">Оновлення замовлення архітектури</a></li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="team" class="team team-custom  team-main">
        <div class="container">
            <div class="team-details">
                <div class="team-card team-card-custom">
                    <div class="container">
                        <div class="row">
                            <h4 class="custom-middle-margin">Оновлення замовлення архітектури</h4>

                            <form method="post"  class="custom-middle-margin">
                                <label for="surname">Прізвище</label>
                                <input type="text" name="surname" class="form-control" value="<?php echo($order['surname']); ?>">

                                <br>

                                <label for="name">Ім'я</label>
                                <input type="text" name="name" class="form-control" value="<?php echo($order['name']); ?>">

                                <br>



                                <label for="phone">Телефон</label>
                                <input type="text" name="phone" class="form-control" value="<?php echo($order['phone']); ?>">

                                <br>

                                <label for="architecture_id">Код роботи</label>
                                <input type="number" name="architecture_id" class="form-control" value="<?php echo($order['architecture_id']); ?>">

                                <br>

                                <label for="user_message">Повідомлення користувача</label>
                                <input type="text" name="user_message" class="form-control" value="<?php echo($order['user_message']); ?>">

                                <br>

                                <button type="submit" name="submit" class="custom-btn-admin">Оновити</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
<?php include 'views/layouts/footer_admin.php'; ?>
</body>
</html>