<?php
class AdminTestimonialsController extends AdminBase
{
    public function actionIndex()
    {
        self::checkAdmin();
        $testimonialsList = Testimonial::getAllTestimonials();
        require_once('views/admin_testimonials/index.php');
        return true;
    }

    public function actionDelete($id) {
        self::checkAdmin();

        $testimonial = Testimonial::getTestimonialById($id);

        if(!$testimonial) header("Location: /404");

        if(isset($_POST['submit'])) {
            Testimonial::deleteTestimonialById($id);
            header("Location: /admin/testimonials/");
        }
        require_once('views/admin_testimonials/delete.php');
        return true;
    }

    public static function actionCreate() {
        self::checkAdmin();

        if (isset($_POST['submit'])) {
            $options['name'] = $_POST['name'];
            $options['surname'] = $_POST['surname'];
            $options['testimonial'] = $_POST['testimonial'];

            $errors = false;

            if (!isset($options['name']) || empty($options['name'])) {
                $errors[] = 'Заповніть поля';
            }

            if ($errors == false) {

                $id = Testimonial::createTestimonial($options);

                header("Location: /admin/testimonials");
            }
        }

        require_once('views/admin_testimonials/create.php');
        return true;
    }

    public function actionUpdate($id) {
        self::checkAdmin();

        $testimonial = Testimonial::getTestimonialById($id);

        if(!$testimonial) header("Location: /404");

        if(isset($_POST['submit'])) {
            $options['name'] = $_POST['name'];
            $options['surname'] = $_POST['surname'];
            $options['testimonial'] = $_POST['testimonial'];

            Testimonial::updateTestimonialById($id, $options);

            header("Location: /admin/testimonials");
        }
        require_once('views/admin_testimonials/update.php');
        return true;
    }


}

?>