<?php
class CabinetController extends AdminBase {
    public function actionIndex() {
        $isAdmin = self::checkAdminTrueFalse();

        $userId = User::checkLogged();

        $user = User::getUserById($userId);

        if(!$user) header("Location: /404");

        require_once('views/cabinet/index.php');

        return true;
    }

    public function actionEdit() {
        $userId = User::checkLogged();

        $user = User::getUserById($userId);

        if(!$user) header("Location: /404");

        $name = $user['name'];
        $email = $user['email'];
        $password = $user['password'];

        $result = false;
        if(isset($_POST['submit'])) {
            $name = $_POST['name'];
            $password = $_POST['password'];
            $email = $_POST['email'];

            $errors = false;

            if(!User::checkName($name)) {
                $errors[] = "Ім'я не повинно бути коротшим за 2 символа";
            }

            if(!User::checkPassword($password)) {
                $errors[] = 'Пароль не повинен бути коротшим за 6 символів';
            }

            if($errors == false) {
                $result = User::edit($userId, $name, $email, $password);
            }
        }

        require_once('views/cabinet/edit.php');

        return true;
    }
}
?>