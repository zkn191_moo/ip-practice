<?php
class AdminOrdersRepairController extends AdminBase
{
    public function actionIndex()
    {
        self::checkAdmin();

        $ordersRepairList = Repair::getOrdersRepair();

        require_once('views/admin_orders_repair/index.php');
        return true;
    }

    public function actionDelete($id) {
        self::checkAdmin();

        $order = Repair::getRepairOrderById($id);

        if(!$order) header("Location: /404");

        if(isset($_POST['submit'])) {
            Repair::deleteRepairOrderById($id);

            header("Location: /admin/orders_repair/");
        }
        require_once('views/admin_orders_repair/delete.php');
        return true;
    }



    public static function actionCreate() {
        self::checkAdmin();

        if (isset($_POST['submit'])) {
            $options['surname'] = $_POST['surname'];
            $options['name'] = $_POST['name'];
            $options['phone'] = $_POST['phone'];
            $options['repair_id'] = $_POST['repair_id'];
            $options['user_message'] = $_POST['user_message'];


            $errors = false;

            if (!isset($options['name']) || empty($options['name'])) {
                $errors[] = 'Заповніть поля';
            }

            if ($errors == false) {

                $id = Repair::createRepairOrder($options);

                header("Location: /admin/orders_repair/");
            }
        }

        require_once('views/admin_orders_repair/create.php');
        return true;
    }

    public function actionUpdate($id) {
        self::checkAdmin();

        $order = Repair::getRepairOrderById($id);

        if(!$order) header("Location: /404");

        if(isset($_POST['submit'])) {
            $options['surname'] = $_POST['surname'];
            $options['name'] = $_POST['name'];
            $options['phone'] = $_POST['phone'];
            $options['repair_id'] = $_POST['repair_id'];
            $options['user_message'] = $_POST['user_message'];

            Repair::updateRepairOrderById($id, $options);

            header("Location: /admin/orders_repair/");
        }
        require_once('views/admin_orders_repair/update.php');
        return true;
    }
}