<?php
class AdminServiceArchitectureController extends AdminBase
{
    public function actionIndex()
    {
        self::checkAdmin();

        $serviceArchitectureList = Architecture::getServiceArchitecture();
//        $serviceArchitectureList = Repair::getOrdersRepair();

        require_once('views/admin_service_architecture/index.php');
        return true;
    }

    public function actionDelete($id) {
        self::checkAdmin();

        $service = Architecture::getArchitectureServiceById($id);
//        $order = Repair::getRepairOrderById($id);

        if(!$service) header("Location: /404");

        if(isset($_POST['submit'])) {

            Architecture::deleteArchitectureServiceById($id);
//            Repair::deleteRepairOrderById($id);

            header("Location: /admin/service_architecture/");
        }
        require_once('views/admin_service_architecture/delete.php');
        return true;
    }

    public static function actionCreate() {
        self::checkAdmin();

        if (isset($_POST['submit'])) {

            $options['name'] = $_POST['name'];
            $options['internal_external'] = $_POST['internal_external'];
            $options['work_type'] = $_POST['work_type'];
            $options['work_dimension'] = $_POST['work_dimension'];
            $options['price'] = $_POST['price'];
            $options['description'] = $_POST['description'];
            $options['is_showing'] = $_POST['is_showing'];
            $options['is_advertising'] = $_POST['is_advertising'];


            $errors = false;

            if (!isset($options['name']) || empty($options['name'])) {
                $errors[] = 'Заповніть поля';
            }

            if ($errors == false) {

                $id = Architecture::createArchitectureService($options);

                header("Location: /admin/service_architecture/");
            }
        }

        require_once('views/admin_service_architecture/create.php');
        return true;
    }

    public function actionUpdate($id) {
        self::checkAdmin();

        $service = Architecture::getArchitectureServiceById($id);

        if(!$service) header("Location: /404");

        if(isset($_POST['submit'])) {
            $options['name'] = $_POST['name'];
            $options['internal_external'] = $_POST['internal_external'];
            $options['work_type'] = $_POST['work_type'];
            $options['work_dimension'] = $_POST['work_dimension'];
            $options['price'] = $_POST['price'];
            $options['description'] = $_POST['description'];
            $options['is_showing'] = $_POST['is_showing'];
            $options['is_advertising'] = $_POST['is_advertising'];

            Architecture::updateArchitectureServiceById($id, $options);

            header("Location: /admin/service_architecture/");
        }
        require_once('views/admin_service_architecture/update.php');
        return true;
    }
}
?>