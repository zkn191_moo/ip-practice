<?php
class AdminProjectsController extends AdminBase
{
    public function actionIndex()
    {
        self::checkAdmin();
        $projectsList = Projects::getFinishedProjects();
        require_once('views/admin_projects/index.php');
        return true;
    }

    public function actionDelete($id) {
        self::checkAdmin();

        $projectTest = Projects::getProjectById($id);

        if(!$projectTest) header("Location: /404");

        if(isset($_POST['submit'])) {
            Projects::deleteProjectById($id);
            header("Location: /admin/projects/");
        }
        require_once('views/admin_projects/delete.php');
        return true;
    }

    public static function actionCreate() {
        self::checkAdmin();

        if (isset($_POST['submit'])) {
            $options['name'] = $_POST['name'];
            $options['client'] = $_POST['client'];
            $options['contract_date'] = $_POST['contract_date'];
            $options['money_amount'] = $_POST['money_amount'];


            $errors = false;

            if (!isset($options['name']) || empty($options['name'])) {
                $errors[] = 'Заповніть поля';
            }

            if ($errors == false) {

                $id = Projects::createProject($options);

                header("Location: /admin/projects");
            }
        }

        require_once('views/admin_projects/create.php');
        return true;
    }

    public function actionUpdate($id) {
        self::checkAdmin();
        $project = Projects::getProjectById($id);

        if(!$project) header("Location: /404");

        if(isset($_POST['submit'])) {
            $options['name'] = $_POST['name'];
            $options['client'] = $_POST['client'];
            $options['contract_date'] = $_POST['contract_date'];
            $options['money_amount'] = $_POST['money_amount'];

            Projects::updateProjectById($id, $options);

            header("Location: /admin/projects");
        }
        require_once('views/admin_projects/update.php');
        return true;
    }
}

?>