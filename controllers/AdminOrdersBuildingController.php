<?php
class AdminOrdersBuildingController extends AdminBase
{
    public function actionIndex()
    {
        self::checkAdmin();

        $ordersBuildingList = Building::getOrdersBuilding();

        require_once('views/admin_orders_building/index.php');
        return true;
    }

    public function actionDelete($id) {
        self::checkAdmin();

        $order = Building::getBuildingOrderById($id);

        if(!$order) header("Location: /404");

        if(isset($_POST['submit'])) {

            Building::deleteBuildingOrderById($id);

            header("Location: /admin/orders_building/");
        }
        require_once('views/admin_orders_building/delete.php');
        return true;
    }



    public static function actionCreate() {
        self::checkAdmin();

        if (isset($_POST['submit'])) {
            $options['surname'] = $_POST['surname'];
            $options['name'] = $_POST['name'];
            $options['phone'] = $_POST['phone'];
            $options['building_id'] = $_POST['building_id'];
            $options['user_message'] = $_POST['user_message'];


            $errors = false;

            if (!isset($options['name']) || empty($options['name'])) {
                $errors[] = 'Заповніть поля';
            }

            if ($errors == false) {

                $id = Building::createBuildingOrder($options);

                header("Location: /admin/orders_building/");
            }
        }

        require_once('views/admin_orders_building/create.php');
        return true;
    }

    public function actionUpdate($id) {
        self::checkAdmin();

        $order = Building::getBuildingOrderById($id);

        if(!$order) header("Location: /404");

        if(isset($_POST['submit'])) {
            $options['surname'] = $_POST['surname'];
            $options['name'] = $_POST['name'];
            $options['phone'] = $_POST['phone'];
            $options['building_id'] = $_POST['building_id'];
            $options['user_message'] = $_POST['user_message'];

            Building::updateBuildingOrderById($id, $options);

            header("Location: /admin/orders_building/");
        }
        require_once('views/admin_orders_building/update.php');
        return true;
    }
}