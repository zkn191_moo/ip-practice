<?php
class AdminServiceRepairController extends AdminBase
{
    public function actionIndex()
    {
        self::checkAdmin();

        $serviceRepairList = Repair::getServiceRepair();

        require_once('views/admin_service_repair/index.php');
        return true;
    }

    public function actionDelete($id) {
        self::checkAdmin();

        $service = Repair::getRepairServiceById($id);

        if(!$service) header("Location: /404");

        if(isset($_POST['submit'])) {

            Repair::deleteRepairServiceById($id);

            header("Location: /admin/service_repair/");
        }
        require_once('views/admin_service_repair/delete.php');
        return true;
    }

    public static function actionCreate() {
        self::checkAdmin();

        if (isset($_POST['submit'])) {

            $options['name'] = $_POST['name'];
            $options['internal_external'] = $_POST['internal_external'];
            $options['work_type'] = $_POST['work_type'];
            $options['work_dimension'] = $_POST['work_dimension'];
            $options['price'] = $_POST['price'];
            $options['description'] = $_POST['description'];
            $options['is_showing'] = $_POST['is_showing'];
            $options['is_advertising'] = $_POST['is_advertising'];


            $errors = false;

            if (!isset($options['name']) || empty($options['name'])) {
                $errors[] = 'Заповніть поля';
            }

            if ($errors == false) {

                $id = Repair::createRepairService($options);

                header("Location: /admin/service_repair/");
            }
        }

        require_once('views/admin_service_repair/create.php');
        return true;
    }

    public function actionUpdate($id) {
        self::checkAdmin();

        $service = Repair::getRepairServiceById($id);

        if(!$service) header("Location: /404");

        if(isset($_POST['submit'])) {
            $options['name'] = $_POST['name'];
            $options['internal_external'] = $_POST['internal_external'];
            $options['work_type'] = $_POST['work_type'];
            $options['work_dimension'] = $_POST['work_dimension'];
            $options['price'] = $_POST['price'];
            $options['description'] = $_POST['description'];
            $options['is_showing'] = $_POST['is_showing'];
            $options['is_advertising'] = $_POST['is_advertising'];

            Repair::updateRepairServiceById($id, $options);

            header("Location: /admin/service_repair/");
        }
        require_once('views/admin_service_repair/update.php');
        return true;
    }
}
?>