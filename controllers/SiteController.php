<?php

    class SiteController {
        public function actionIndex() {

            $finishedProjectsList = Projects::getFinishedProjects();

            $userName = '';
            $userSurname = '';
            $userTestimonial = '';


            if(isset($_POST['submit'])) {
                $userName = $_POST['userName'];
                $userSurname = $_POST['userSurname'];
                $userTestimonial = $_POST['userTestimonial'];

                Testimonial::createTestimonial($userName, $userSurname, $userTestimonial);

                header("Location: /");

            }

            $testimonials = array();
            $testimonials = Testimonial::getAllTestimonials();

            $allAdvertisingServices = Repair::getAllAdvertisingServices();

            require_once('views/site/index.php');
            return true;
        }

        public function actionContact() {
            $userEmail = '';
            $userText = '';
            $userName = '';
            $userSurname = '';
            $userPhone = '';
            $result = false;

            if(isset($_POST['submit'])) {
                $userEmail = $_POST['userEmail'];
                $userText = $_POST['userText'];
                $userName = $_POST['userName'];
                $userSurname = $_POST['userSurname'];
                $userPhone = $_POST['userPhone'];

                $errors = false;

                if (!User::checkEmail($userEmail)) {
                    $errors[] = 'Неправильний email';
                }
                if ($errors == false) {
                    $typeOfEmail = "contact with us";
                    require_once('mail.php');
                }
            }

            require_once('views/site/contact.php');
            return true;
        }

        public function actionAbout() {
            require_once('views/site/about.php');
            return true;
        }

        public function actionProjects(){
            $finishedProjectsList = Projects::getFinishedProjects();
            require_once('views/site/projects.php');
            return true;
        }

        public function actionRepair(){
            $repairWorkList = Repair::getRepairWorkList();

            if (isset($_POST['submit'])) {

                $options['userSurname'] = $_POST['userSurname'];
                $options['userName'] = $_POST['userName'];
                $options['userPhone'] = $_POST['userPhone'];
                $options['repair_id'] = $_POST['repair_id'];
                $options['userText'] = $_POST['userText'];

                $errors = false;

                if (!isset($options['userSurname']) || empty($options['userName'])) {
                    $errors[] = 'Заповніть поля';
                }

                if ($errors == false) {
                    Order::createNewOrderRepair($options);
                }
            }

            require_once('views/site/repair.php');
            return true;
        }

        public function actionBuilding(){
            $buildingWorkList = Building::getBuildingWorkList();

            if (isset($_POST['submit'])) {

                $options['userSurname'] = $_POST['userSurname'];
                $options['userName'] = $_POST['userName'];
                $options['userPhone'] = $_POST['userPhone'];
                $options['building_id'] = $_POST['building_id'];
                $options['userText'] = $_POST['userText'];


                $errors = false;

                if (!isset($options['userSurname']) || empty($options['userName'])) {
                    $errors[] = 'Заповніть поля';
                }

                if ($errors == false) {

                    Order::createNewOrderBuilding($options);

                }
            }

            require_once('views/site/building.php');
            return true;
        }

        public function actionArchitecture(){
            $architectureWorkList = Architecture::getArchitectureWorkList();

            if (isset($_POST['submit'])) {

                $options['userSurname'] = $_POST['userSurname'];
                $options['userName'] = $_POST['userName'];
                $options['userPhone'] = $_POST['userPhone'];
                $options['architecture_id'] = $_POST['architecture_id'];
                $options['userText'] = $_POST['userText'];


                $errors = false;

                if (!isset($options['userSurname']) || empty($options['userName'])) {
                    $errors[] = 'Заповніть поля';
                }

                if ($errors == false) {

                    Order::createNewOrderArchitecture($options);

                }
            }

            require_once('views/site/architecture.php');
            return true;
        }
    }
?>


