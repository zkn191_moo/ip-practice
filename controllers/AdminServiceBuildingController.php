<?php
class AdminServiceBuildingController extends AdminBase
{
    public function actionIndex()
    {
        self::checkAdmin();

        $serviceBuildingList = Building::getServiceBuilding();

        require_once('views/admin_service_building/index.php');
        return true;
    }

    public function actionDelete($id) {
        self::checkAdmin();

        $service = Building::getBuildingServiceById($id);

        if(!$service) header("Location: /404");

        if(isset($_POST['submit'])) {

            Building::deleteBuildingServiceById($id);

            header("Location: /admin/service_building/");
        }
        require_once('views/admin_service_building/delete.php');
        return true;
    }

    public static function actionCreate() {
        self::checkAdmin();

        if (isset($_POST['submit'])) {

            $options['name'] = $_POST['name'];
            $options['internal_external'] = $_POST['internal_external'];
            $options['work_type'] = $_POST['work_type'];
            $options['work_dimension'] = $_POST['work_dimension'];
            $options['price'] = $_POST['price'];
            $options['description'] = $_POST['description'];
            $options['is_showing'] = $_POST['is_showing'];
            $options['is_advertising'] = $_POST['is_advertising'];

            $errors = false;

            if (!isset($options['name']) || empty($options['name'])) {
                $errors[] = 'Заповніть поля';
            }

            if ($errors == false) {

                $id = Building::createBuildingService($options);

                header("Location: /admin/service_building/");
            }
        }

        require_once('views/admin_service_building/create.php');
        return true;
    }

    public function actionUpdate($id) {
        self::checkAdmin();

        $service = Building::getBuildingServiceById($id);

        if(!$service) header("Location: /404");

        if(isset($_POST['submit'])) {
            $options['name'] = $_POST['name'];
            $options['internal_external'] = $_POST['internal_external'];
            $options['work_type'] = $_POST['work_type'];
            $options['work_dimension'] = $_POST['work_dimension'];
            $options['price'] = $_POST['price'];
            $options['description'] = $_POST['description'];
            $options['is_showing'] = $_POST['is_showing'];
            $options['is_advertising'] = $_POST['is_advertising'];

            Building::updateBuildingServiceById($id, $options);

            header("Location: /admin/service_building/");
        }
        require_once('views/admin_service_building/update.php');
        return true;
    }
}
?>