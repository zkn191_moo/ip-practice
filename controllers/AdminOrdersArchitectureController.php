<?php
class AdminOrdersArchitectureController extends AdminBase
{
    public function actionIndex()
    {
        self::checkAdmin();
        $ordersArchitectureList = Architecture::getOrdersArchitecture();

        require_once('views/admin_orders_architecture/index.php');
        return true;
    }

    public function actionDelete($id) {
        self::checkAdmin();

        $order = Architecture::getArchitectureOrderById($id);

        if(!$order) header("Location: /404");

        if(isset($_POST['submit'])) {
            Architecture::deleteArchitectureOrderById($id);

            header("Location: /admin/orders_architecture/");
        }
        require_once('views/admin_orders_architecture/delete.php');
        return true;
    }

    public static function actionCreate() {
        self::checkAdmin();

        if (isset($_POST['submit'])) {
            $options['surname'] = $_POST['surname'];
            $options['name'] = $_POST['name'];
            $options['phone'] = $_POST['phone'];
            $options['architecture_id'] = $_POST['architecture_id'];
            $options['user_message'] = $_POST['user_message'];


            $errors = false;

            if (!isset($options['name']) || empty($options['name'])) {
                $errors[] = 'Заповніть поля';
            }

            if ($errors == false) {

                $id = Architecture::createArchitectureOrder($options);

                header("Location: /admin/orders_architecture/");
            }
        }

        require_once('views/admin_orders_architecture/create.php');
        return true;
    }

    public function actionUpdate($id) {
        self::checkAdmin();

        $order = Architecture::getArchitectureOrderById($id);

        if(!$order) header("Location: /404");

        if(isset($_POST['submit'])) {
            $options['surname'] = $_POST['surname'];
            $options['name'] = $_POST['name'];
            $options['phone'] = $_POST['phone'];
            $options['architecture_id'] = $_POST['architecture_id'];
            $options['user_message'] = $_POST['user_message'];

            Architecture::updateArchitectureOrderById($id, $options);

            header("Location: /admin/orders_architecture/");
        }
        require_once('views/admin_orders_architecture/update.php');
        return true;
    }
}