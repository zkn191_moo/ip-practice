<?php

class UserController {


    public function actionLogin() {
        $email = '';
        $password = '';

        if(isset($_POST['submit'])) {
            $email = $_POST['email'];
            $password = $_POST['password'];

            $errors = false;

            if(!User::checkEmail($email)) {
                $errors[] = 'Неправильний email';
            }
            if(!User::checkPassword($password)) {
                $errors[] = 'Пароль не повинен бути коротшим 6-ти символів';
            }

            $userId = User::checkUserData($email, $password);

            if($userId == false) {
                $errors[] = 'Неправильні дані для входу на сайт';
            }
            else {
                User::auth($userId);
                header("Location: /cabinet/");
            }
        }
        require_once('views/user/login.php');

        return true;
    }

    public function actionLogout() {
        unset($_SESSION["userLoggedId"]);
        header("Location: /");
    }
}



?>