<?php
    return array(

        'user/login' => 'user/login',  // actionLogin у UserController
        'user/logout' => 'user/logout',  // actionLogout у UserController

        'cabinet/edit' => 'cabinet/edit',  // actionEdit у CabinetController
        'cabinet' => 'cabinet/index',  // actionIndex у CabinetController



        'admin/projects/create' => 'adminProjects/create',  // actionCreate у AdminProjectsController
        'admin/projects/update/([0-9]+)' => 'adminProjects/update/$1',  // actionUpdate у AdminProjectsController
        'admin/projects/delete/([0-9]+)' => 'adminProjects/delete/$1',  // actionDelete у AdminProjectsController
        'admin/projects' => 'adminProjects/index',  // actionIndex у AdminProjectsController


        'admin/testimonials/create' => 'adminTestimonials/create',  // actionCreate у AdminTestimonialsController
        'admin/testimonials/update/([0-9]+)' => 'adminTestimonials/update/$1',  // actionUpdate у AdminTestimonialsController
        'admin/testimonials/delete/([0-9]+)' => 'adminTestimonials/delete/$1',  // actionDelete у AdminTestimonialsController
        'admin/testimonials' => 'adminTestimonials/index',  // actionIndex у AdminTestimonialsController



        'admin/orders_architecture/create' => 'AdminOrdersArchitecture/create',  // actionCreate у AdminOrdersArchitectureController
        'admin/orders_architecture/update/([0-9]+)' => 'AdminOrdersArchitecture/update/$1',  // actionUpdate у AdminOrdersArchitectureController
        'admin/orders_architecture/delete/([0-9]+)' => 'AdminOrdersArchitecture/delete/$1',  // actionDelete у AdminOrdersArchitectureController
        'admin/orders_architecture' => 'AdminOrdersArchitecture/index',  // actionIndex у AdminOrdersArchitectureController


        'admin/orders_building/create' => 'AdminOrdersBuilding/create',  // actionCreate у AdminOrdersBuildingController
        'admin/orders_building/update/([0-9]+)' => 'AdminOrdersBuilding/update/$1',  // actionUpdate у AdminOrdersBuildingController
        'admin/orders_building/delete/([0-9]+)' => 'AdminOrdersBuilding/delete/$1',  // actionDelete у AdminOrdersBuildingController
        'admin/orders_building' => 'AdminOrdersBuilding/index',  // actionIndex у AdminOrdersBuildingController


        'admin/orders_repair/create' => 'AdminOrdersRepair/create',  // actionCreate у AdminOrdersRepairController
        'admin/orders_repair/update/([0-9]+)' => 'AdminOrdersRepair/update/$1',  // actionUpdate у AdminOrdersRepairController
        'admin/orders_repair/delete/([0-9]+)' => 'AdminOrdersRepair/delete/$1',  // actionDelete у AdminOrdersRepairController
        'admin/orders_repair' => 'AdminOrdersRepair/index',  // actionIndex у AdminOrdersRepairController



        'admin/service_architecture/create' => 'AdminServiceArchitecture/create',  // actionCreate у AdminServiceArchitectureController
        'admin/service_architecture/update/([0-9]+)' => 'AdminServiceArchitecture/update/$1',  // actionUpdate у AdminServiceArchitectureController
        'admin/service_architecture/delete/([0-9]+)' => 'AdminServiceArchitecture/delete/$1',  // actionDelete у AdminServiceArchitectureController
        'admin/service_architecture' => 'AdminServiceArchitecture/index',  // actionIndex у AdminServiceArchitectureController


        'admin/service_building/create' => 'AdminServiceBuilding/create',  // actionCreate у AdminServiceBuildingController
        'admin/service_building/update/([0-9]+)' => 'AdminServiceBuilding/update/$1',  // actionUpdate у AdminServiceBuildingController
        'admin/service_building/delete/([0-9]+)' => 'AdminServiceBuilding/delete/$1',  // actionDelete у AdminServiceBuildingController
        'admin/service_building' => 'AdminServiceBuilding/index',  // actionIndex у AdminServiceBuildingController


        'admin/service_repair/create' => 'AdminServiceRepair/create',  // actionCreate у AdminServiceRepairController
        'admin/service_repair/update/([0-9]+)' => 'AdminServiceRepair/update/$1',  // actionUpdate у AdminServiceRepairController
        'admin/service_repair/delete/([0-9]+)' => 'AdminServiceRepair/delete/$1',  // actionDelete у AdminServiceRepairController
        'admin/service_repair' => 'AdminServiceRepair/index',  // actionIndex у AdminServiceRepairController

        'admin' => 'admin/index',  // actionIndex у AdminController

        'contacts' => 'site/contact',  // actionContact у SiteController
        'about' => 'site/about',  // actionAbout у SiteController
        'projects' => 'site/projects',  // actionProjects у SiteController
        'repair' => 'site/repair',  // actionRepair у SiteController
        'building' => 'site/building',  // actionBuilding у SiteController
        'architecture' => 'site/architecture',  // actionArchitecture у SiteController

        '404' => 'error/index',  //actionIndex у ErrorController

        '' => 'site/index', // actionIndex у SiteController
    );
?>