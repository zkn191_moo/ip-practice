<?php
abstract class AdminBase {
    public static function checkAdmin() {
        $userId = User::checkLogged();
        $user = User::getUserById($userId);

        if(!$user) header("Location: /404");

        if($user['role'] == 'admin') {
            return true;
        }
        die("Доступ заборонений!");
    }

    public static function checkAdminTrueFalse() {
        $userId = User::checkLogged();
        $user = User::getUserById($userId);

        if(!$user) header("Location: /404");

        if($user['role'] == 'admin') {
            return true;
        }
        return false;
    }
}
?>