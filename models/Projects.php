<?php
class Projects {
    public static function getFinishedProjects() {
        $db = Db::getConnection();
        $result = $db->query('SELECT * FROM finished_projects ORDER BY contract_date ASC');
        $projectsList = array();

        $i = 0;
        while ($row = $result->fetch()) {
            $projectsList[$i]['id'] = $row['id'];
            $projectsList[$i]['name'] = $row['name'];
            $projectsList[$i]['client'] = $row['client'];
            $projectsList[$i]['contract_date'] = $row['contract_date'];
            $projectsList[$i]['money_amount'] = $row['money_amount'];
            $i++;
        }
        return $projectsList;
    }

    public static function getProjectById($id)
    {
        $id = intval($id);

        if ($id) {
            $db = Db::getConnection();

            $result = $db->query('SELECT * FROM finished_projects WHERE id=' . $id);
            $result->setFetchMode(PDO::FETCH_ASSOC);

            return $result->fetch();
        }
    }

    public static function deleteProjectById($id) {
        $db = Db::getConnection();
        $sql = 'DELETE FROM finished_projects WHERE id = :id';
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        return $result->execute();
    }

    public static function createProject($options) {
        $db = Db::getConnection();

        $sql = 'INSERT INTO finished_projects ' .
            '(name, client, contract_date, money_amount) ' .
            'VALUES ' .
            '(:name, :client, :contract_date, :money_amount) ';

        $result = $db->prepare($sql);
        $result->bindParam(':name', $options['name'], PDO::PARAM_STR);
        $result->bindParam(':client', $options['client'], PDO::PARAM_STR);
        $result->bindParam(':contract_date', $options['contract_date'], PDO::PARAM_STR);
        $result->bindParam(':money_amount', $options['money_amount']);

        if($result->execute()) {
            return $db->lastInsertId();
        }
        return 0;

    }


    public static function updateProjectById($id, $options) {
        $db = Db::getConnection();

        $sql = "UPDATE finished_projects SET name = :name, client = :client, 
                contract_date = :contract_date, money_amount = :money_amount WHERE id = :id";

        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->bindParam(':name', $options['name'], PDO::PARAM_STR);
        $result->bindParam(':client', $options['client'], PDO::PARAM_STR);
        $result->bindParam(':contract_date', $options['contract_date'], PDO::PARAM_STR);
        $result->bindParam(':money_amount', $options['money_amount']);

        return $result->execute();
    }
}

?>