<?php
class Order {
    public static function createNewOrderRepair($options) {
        $db = Db::getConnection();

        $sql = "INSERT INTO `repair_orders` (`suname`, `name`, `phone`, `repair_id`, `user_message`) VALUES (:surname, :name, :phone, :repair_id, :user_message);";

        $result = $db->prepare($sql);
        $result->bindParam(':surname', $options['userSurname'], PDO::PARAM_STR);
        $result->bindParam(':name', $options['userName'], PDO::PARAM_STR);
        $result->bindParam(':phone', $options['userPhone'], PDO::PARAM_STR);
        $result->bindParam(':repair_id', $options['repair_id'], PDO::PARAM_INT);
        $result->bindParam(':user_message', $options['userText']);

        $result->execute();

    }

    public static function createNewOrderBuilding($options) {
        $db = Db::getConnection();

        $sql = "INSERT INTO `building_orders` (`suname`, `name`, `phone`, `building_id`, `user_message`) VALUES (:surname, :name, :phone, :building_id, :user_message);";

        $result = $db->prepare($sql);
        $result->bindParam(':surname', $options['userSurname'], PDO::PARAM_STR);
        $result->bindParam(':name', $options['userName'], PDO::PARAM_STR);
        $result->bindParam(':phone', $options['userPhone'], PDO::PARAM_STR);
        $result->bindParam(':building_id', $options['building_id'], PDO::PARAM_INT);
        $result->bindParam(':user_message', $options['userText']);

        $result->execute();

    }


    public static function createNewOrderArchitecture($options) {
        $db = Db::getConnection();

        $sql = "INSERT INTO `architecture_orders` (`surname`, `name`, `phone`, `architecture_id`, `user_message`) VALUES (:surname, :name, :phone, :architecture_id, :user_message);";

        $result = $db->prepare($sql);
        $result->bindParam(':surname', $options['userSurname'], PDO::PARAM_STR);
        $result->bindParam(':name', $options['userName'], PDO::PARAM_STR);
        $result->bindParam(':phone', $options['userPhone'], PDO::PARAM_STR);
        $result->bindParam(':architecture_id', $options['architecture_id'], PDO::PARAM_INT);
        $result->bindParam(':user_message', $options['userText']);

        $result->execute();

    }














    public static function save($userSurname, $userName, $userPatronymic, $userPhone, $userEmail, $userComment, $userId, $products) {
        $db = Db::getConnection();

        $products = json_encode($products);

        $sql = 'INSERT INTO product_order (user_surname, user_name, user_patronymic, user_phone, user_email, user_comment, user_id, products)' .
            'VALUES (:user_surname, :user_name, :user_patronymic, :user_phone, :user_email, :user_comment, :user_id, :products)';

        $result = $db->prepare($sql);
        $result->bindParam(':user_surname', $userSurname);
        $result->bindParam(':user_name', $userName);
        $result->bindParam(':user_patronymic', $userPatronymic);
        $result->bindParam(':user_phone', $userPhone);
        $result->bindParam(':user_email', $userEmail);
        $result->bindParam(':user_comment', $userComment);
        $result->bindParam(':user_id', $userId, PDO::PARAM_INT);
        $result->bindParam(':products', $products);

        return $result->execute();
    }

    public static function getOrderById($id) {
        $db = Db::getConnection();
        $sql = 'SELECT * FROM product_order WHERE id = :id';
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $result->execute();
        return $result->fetch();
    }

    public static function getStatusText($status) {
        switch ($status) {
            case '1':
                return 'Нове замовлення';
                break;
            case '2':
                return 'В обробці';
                break;
            case '3':
                return 'Доставляється';
                break;
            case '4':
                return 'Зачинений';
                break;
        }
    }

    public static function getOrdersList() {
        $db = Db::getConnection();

        $result = $db->query('SELECT id, user_surname, user_name, user_patronymic, user_phone, user_email, date, status FROM product_order ORDER BY id DESC');
        $ordersList = array();
        $i = 0;
        while ($row = $result->fetch()) {
            $ordersList[$i]['id'] = $row['id'];
            $ordersList[$i]['user_surname'] = $row['user_surname'];
            $ordersList[$i]['user_name'] = $row['user_name'];
            $ordersList[$i]['user_patronymic'] = $row['user_patronymic'];
            $ordersList[$i]['user_phone'] = $row['user_phone'];
            $ordersList[$i]['user_email'] = $row['user_email'];
            $ordersList[$i]['date'] = $row['date'];
            $ordersList[$i]['status'] = $row['status'];
            $i++;
        }
        return $ordersList;
    }

    public static function deleteOrderById($id) {
        $db = Db::getConnection();

        $sql = 'DELETE FROM product_order WHERE id = :id';

        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        return $result->execute();
    }

    public static function updateOrderById($id, $userSurname, $userName, $userPatronymic, $userPhone, $userEmail, $userComment, $status) {
        $db = Db::getConnection();

        $sql = "UPDATE product_order
            SET 
                user_surname = :user_surname, 
                user_name = :user_name, 
                user_patronymic = :user_patronymic, 
                user_phone = :user_phone, 
                user_email = :user_email, 
                user_comment = :user_comment, 
               
                status = :status 
            WHERE id = :id";

        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->bindParam(':user_surname', $userSurname, PDO::PARAM_STR);
        $result->bindParam(':user_name', $userName, PDO::PARAM_STR);
        $result->bindParam(':user_patronymic', $userPatronymic, PDO::PARAM_STR);
        $result->bindParam(':user_phone', $userPhone, PDO::PARAM_STR);
        $result->bindParam(':user_email', $userEmail, PDO::PARAM_STR);
        $result->bindParam(':user_comment', $userComment, PDO::PARAM_STR);

        $result->bindParam(':status', $status, PDO::PARAM_INT);
        return $result->execute();
    }
}
?>