<?php

class Repair
{
    public static function getRepairWorkList()
    {
        $db = Db::getConnection();
        $result = $db->query('SELECT * FROM repair_services WHERE is_showing = "1" ORDER BY name ASC');
        $repairList = array();

        $i = 0;
        while ($row = $result->fetch()) {
            $repairList[$i]['id'] = $row['id'];
            $repairList[$i]['name'] = $row['name'];
            $repairList[$i]['internal_external'] = $row['internal_external'];
            $repairList[$i]['work_type'] = $row['work_type'];
            $repairList[$i]['dimension'] = $row['dimension'];
            $repairList[$i]['price'] = $row['price'];
            $repairList[$i]['description'] = $row['description'];
            $i++;
        }
        return $repairList;
    }

    public static function getAllAdvertisingServices(){
        $db = Db::getConnection();
        $result = $db->query('SELECT * FROM repair_services UNION SELECT * FROM building_services UNION SELECT * FROM architecture_services  WHERE is_advertising = "1"  ORDER BY name DESC;');
        $advertisingList = array();

        $i = 0;
        while ($row = $result->fetch()) {
            $advertisingList[$i]['id'] = $row['id'];
            $advertisingList[$i]['name'] = $row['name'];
            $advertisingList[$i]['internal_external'] = $row['internal_external'];
            $advertisingList[$i]['work_type'] = $row['work_type'];
            $advertisingList[$i]['work_dimension'] = $row['work_dimension'];
            $advertisingList[$i]['price'] = $row['price'];
            $advertisingList[$i]['description'] = $row['description'];
            $i++;
        }
        return $advertisingList;
    }

    public static function getOrdersRepair() {
        $db = Db::getConnection();
        $result = $db->query('SELECT * FROM repair_orders ORDER BY id DESC ');
        $ordersList = array();

        $i = 0;
        while ($row = $result->fetch()) {
            $ordersList[$i]['id'] = $row['id'];
            $ordersList[$i]['surname'] = $row['surname'];
            $ordersList[$i]['name'] = $row['name'];
            $ordersList[$i]['phone'] = $row['phone'];
            $ordersList[$i]['repair_id'] = $row['repair_id'];
            $ordersList[$i]['user_message'] = $row['user_message'];
            $i++;
        }
        return $ordersList;
    }

    public static function getRepairOrderById($id)
    {
        $id = intval($id);

        if ($id) {
            $db = Db::getConnection();

            $result = $db->query('SELECT * FROM repair_orders WHERE id=' . $id);
            $result->setFetchMode(PDO::FETCH_ASSOC);

            return $result->fetch();
        }
    }

    public static function deleteRepairOrderById($id) {
        $db = Db::getConnection();
        $sql = 'DELETE FROM repair_orders WHERE id = :id';
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        return $result->execute();
    }
    public static function createRepairOrder($options) {
        $db = Db::getConnection();

        $sql = 'INSERT INTO repair_orders ' .
            '(surname, name, phone, repair_id, user_message) ' .
            'VALUES ' .
            '(:surname, :name, :phone, :repair_id, :user_message) ';

        $result = $db->prepare($sql);
        $result->bindParam(':surname', $options['surname'], PDO::PARAM_STR);
        $result->bindParam(':name', $options['name'], PDO::PARAM_STR);
        $result->bindParam(':phone', $options['phone'], PDO::PARAM_STR);
        $result->bindParam(':repair_id', $options['repair_id'], PDO::PARAM_INT);
        $result->bindParam(':user_message', $options['user_message'], PDO::PARAM_STR);

        if($result->execute()) {
            return $db->lastInsertId();
        }
        return 0;

    }

    public static function updateRepairOrderById($id, $options) {
        $db = Db::getConnection();

        $sql = "UPDATE repair_orders SET surname = :surname, name = :name, phone = :phone, 
                repair_id = :repair_id, user_message = :user_message WHERE id = :id";

        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->bindParam(':surname', $options['surname'], PDO::PARAM_STR);
        $result->bindParam(':name', $options['name'], PDO::PARAM_STR);
        $result->bindParam(':phone', $options['phone'], PDO::PARAM_STR);
        $result->bindParam(':repair_id', $options['repair_id'], PDO::PARAM_INT);
        $result->bindParam(':user_message', $options['user_message']);

        return $result->execute();
    }

    public static function getServiceRepair() {
        $db = Db::getConnection();
        $result = $db->query('SELECT * FROM repair_services ORDER BY id DESC ');
        $serviceList = array();

        $i = 0;
        while ($row = $result->fetch()) {
            $serviceList[$i]['id'] = $row['id'];
            $serviceList[$i]['name'] = $row['name'];
            $serviceList[$i]['internal_external'] = $row['internal_external'];
            $serviceList[$i]['work_type'] = $row['work_type'];
            $serviceList[$i]['work_dimension'] = $row['work_dimension'];
            $serviceList[$i]['price'] = $row['price'];
            $serviceList[$i]['description'] = $row['description'];
            $serviceList[$i]['is_showing'] = $row['is_showing'];
            $serviceList[$i]['is_advertising'] = $row['is_advertising'];

            $i++;
        }
        return $serviceList;
    }

    public static function getRepairServiceById($id)
    {
        $id = intval($id);

        if ($id) {
            $db = Db::getConnection();

            $result = $db->query('SELECT * FROM repair_services WHERE id=' . $id);
            $result->setFetchMode(PDO::FETCH_ASSOC);

            return $result->fetch();
        }
    }

    public static function deleteRepairServiceById($id) {
        $db = Db::getConnection();
        $sql = 'DELETE FROM repair_services WHERE id = :id';
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        return $result->execute();
    }

    public static function createRepairService($options) {
        $db = Db::getConnection();

        $sql = "INSERT INTO `repair_services` (`name`, `internal_external`, `work_type`, `work_dimension`, `price`, `description`, `is_showing`, `is_advertising`) VALUES
        (:name, :internal_external, :work_type, :work_dimension, :price, :description, :is_showing, :is_advertising)";

        $result = $db->prepare($sql);
        $result->bindParam(':name', $options['name'], PDO::PARAM_STR);
        $result->bindParam(':internal_external', $options['internal_external'], PDO::PARAM_STR);
        $result->bindParam(':work_type', $options['work_type'], PDO::PARAM_INT);
        $result->bindParam(':work_dimension', $options['work_dimension'], PDO::PARAM_STR);
        $result->bindParam(':price', $options['price']);
        $result->bindParam(':description', $options['description']);
        $result->bindParam(':is_showing', $options['is_showing'], PDO::PARAM_INT);
        $result->bindParam(':is_advertising', $options['is_advertising'], PDO::PARAM_INT);

        if($result->execute()) {
            return $db->lastInsertId();
        }
        return 0;

    }

    public static function updateRepairServiceById($id, $options) {
        $db = Db::getConnection();

        $sql = "UPDATE `repair_services` SET `name`= :name,`internal_external`= :internal_external,`work_type`= :work_type,`work_dimension`= :work_dimension,`price`= :price,`description`= :description,`is_showing`= :is_showing,`is_advertising`= :is_advertising WHERE `id` = :id ";

        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->bindParam(':name', $options['name'], PDO::PARAM_STR);

        $result->bindParam(':internal_external', $options['internal_external'], PDO::PARAM_STR);
        $result->bindParam(':work_type', $options['work_type'], PDO::PARAM_INT);
        $result->bindParam(':work_dimension', $options['work_dimension'], PDO::PARAM_STR);
        $result->bindParam(':price', $options['price']);
        $result->bindParam(':description', $options['description']);
        $result->bindParam(':is_showing', $options['is_showing'], PDO::PARAM_INT);
        $result->bindParam(':is_advertising', $options['is_advertising'], PDO::PARAM_INT);

        return $result->execute();
    }
}
?>

