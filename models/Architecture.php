<?php

class Architecture
{
    public static function getArchitectureWorkList()
    {
        $db = Db::getConnection();
        $result = $db->query('SELECT * FROM architecture_services WHERE is_showing = "1" ORDER BY name ASC');
        $architectureList = array();

        $i = 0;
        while ($row = $result->fetch()) {
            $architectureList[$i]['id'] = $row['id'];
            $architectureList[$i]['name'] = $row['name'];
            $architectureList[$i]['internal_external'] = $row['internal_external'];
            $architectureList[$i]['work_type'] = $row['work_type'];
            $architectureList[$i]['dimension'] = $row['dimension'];
            $architectureList[$i]['price'] = $row['price'];
            $architectureList[$i]['description'] = $row['description'];
            $i++;
        }
        return $architectureList;
    }

    public static function getOrdersArchitecture() {
        $db = Db::getConnection();
        $result = $db->query('SELECT * FROM architecture_orders ORDER BY id DESC ');
        $ordersList = array();

        $i = 0;
        while ($row = $result->fetch()) {
            $ordersList[$i]['id'] = $row['id'];
            $ordersList[$i]['surname'] = $row['surname'];
            $ordersList[$i]['name'] = $row['name'];
            $ordersList[$i]['phone'] = $row['phone'];
            $ordersList[$i]['architecture_id'] = $row['architecture_id'];
            $ordersList[$i]['user_message'] = $row['user_message'];
            $i++;
        }
        return $ordersList;
    }

    public static function getArchitectureOrderById($id)
    {
        $id = intval($id);

        if ($id) {
            $db = Db::getConnection();

            $result = $db->query('SELECT * FROM architecture_orders WHERE id=' . $id);
            $result->setFetchMode(PDO::FETCH_ASSOC);

            return $result->fetch();
        }
    }

    public static function deleteArchitectureOrderById($id) {
        $db = Db::getConnection();
        $sql = 'DELETE FROM architecture_orders WHERE id = :id';
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        return $result->execute();
    }

    public static function createArchitectureOrder($options) {
        $db = Db::getConnection();

        $sql = 'INSERT INTO architecture_orders ' .
            '(surname, name, phone, architecture_id, user_message) ' .
            'VALUES ' .
            '(:surname, :name, :phone, :architecture_id, :user_message) ';

        $result = $db->prepare($sql);
        $result->bindParam(':surname', $options['surname'], PDO::PARAM_STR);
        $result->bindParam(':name', $options['name'], PDO::PARAM_STR);
        $result->bindParam(':phone', $options['phone'], PDO::PARAM_STR);
        $result->bindParam(':architecture_id', $options['architecture_id'], PDO::PARAM_INT);
        $result->bindParam(':user_message', $options['user_message'], PDO::PARAM_STR);

        if($result->execute()) {
            return $db->lastInsertId();
        }
        return 0;

    }

    public static function updateArchitectureOrderById($id, $options) {
        $db = Db::getConnection();

        $sql = "UPDATE architecture_orders SET surname = :surname, name = :name, phone = :phone, 
                architecture_id = :architecture_id, user_message = :user_message WHERE id = :id";

        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->bindParam(':surname', $options['surname'], PDO::PARAM_STR);
        $result->bindParam(':name', $options['name'], PDO::PARAM_STR);
        $result->bindParam(':phone', $options['phone'], PDO::PARAM_STR);
        $result->bindParam(':architecture_id', $options['architecture_id'], PDO::PARAM_INT);
        $result->bindParam(':user_message', $options['user_message']);

        return $result->execute();
    }

    public static function getServiceArchitecture() {
        $db = Db::getConnection();
        $result = $db->query('SELECT * FROM architecture_services ORDER BY id DESC ');
        $serviceList = array();

        $i = 0;
        while ($row = $result->fetch()) {
            $serviceList[$i]['id'] = $row['id'];
            $serviceList[$i]['name'] = $row['name'];
            $serviceList[$i]['internal_external'] = $row['internal_external'];
            $serviceList[$i]['work_type'] = $row['work_type'];
            $serviceList[$i]['work_dimension'] = $row['work_dimension'];
            $serviceList[$i]['price'] = $row['price'];
            $serviceList[$i]['description'] = $row['description'];
            $serviceList[$i]['is_showing'] = $row['is_showing'];
            $serviceList[$i]['is_advertising'] = $row['is_advertising'];

            $i++;
        }
        return $serviceList;
    }

    public static function getArchitectureServiceById($id)
    {
        $id = intval($id);

        if ($id) {
            $db = Db::getConnection();

            $result = $db->query('SELECT * FROM architecture_services WHERE id=' . $id);
            $result->setFetchMode(PDO::FETCH_ASSOC);

            return $result->fetch();
        }
    }

    public static function deleteArchitectureServiceById($id) {
        $db = Db::getConnection();
        $sql = 'DELETE FROM architecture_services WHERE id = :id';
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        return $result->execute();
    }

    public static function createArchitectureService($options) {
        $db = Db::getConnection();

        $sql = "INSERT INTO `architecture_services` (`name`, `internal_external`, `work_type`, `work_dimension`, `price`, `description`, `is_showing`, `is_advertising`) VALUES
        (:name, :internal_external, :work_type, :work_dimension, :price, :description, :is_showing, :is_advertising)";

        $result = $db->prepare($sql);
        $result->bindParam(':name', $options['name'], PDO::PARAM_STR);
        $result->bindParam(':internal_external', $options['internal_external'], PDO::PARAM_STR);
        $result->bindParam(':work_type', $options['work_type'], PDO::PARAM_INT);
        $result->bindParam(':work_dimension', $options['work_dimension'], PDO::PARAM_STR);
        $result->bindParam(':price', $options['price']);
        $result->bindParam(':description', $options['description']);
        $result->bindParam(':is_showing', $options['is_showing'], PDO::PARAM_INT);
        $result->bindParam(':is_advertising', $options['is_advertising'], PDO::PARAM_INT);

        if($result->execute()) {
            return $db->lastInsertId();
        }
        return 0;

    }

    public static function updateArchitectureServiceById($id, $options) {
        $db = Db::getConnection();

        $sql = "UPDATE `architecture_services` SET `name`= :name,`internal_external`= :internal_external,`work_type`= :work_type,`work_dimension`= :work_dimension,`price`= :price,`description`= :description,`is_showing`= :is_showing,`is_advertising`= :is_advertising WHERE `id` = :id ";

        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->bindParam(':name', $options['name'], PDO::PARAM_STR);

        $result->bindParam(':internal_external', $options['internal_external'], PDO::PARAM_STR);
        $result->bindParam(':work_type', $options['work_type'], PDO::PARAM_INT);
        $result->bindParam(':work_dimension', $options['work_dimension'], PDO::PARAM_STR);
        $result->bindParam(':price', $options['price']);
        $result->bindParam(':description', $options['description']);
        $result->bindParam(':is_showing', $options['is_showing'], PDO::PARAM_INT);
        $result->bindParam(':is_advertising', $options['is_advertising'], PDO::PARAM_INT);

        return $result->execute();
    }
}
?>