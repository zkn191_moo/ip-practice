<?php

class Testimonial
{
    public static function getAllTestimonials()
    {
        $db = Db::getConnection();

        $testimonialsList = array();

        $result = $db->query('SELECT * FROM testimonials '
            . 'ORDER BY id ASC');

        $i = 0;

        if($result != '') {
            while ($row = $result->fetch()) {
                $testimonialsList[$i]['id'] = $row['id'];
                $testimonialsList[$i]['name'] = $row['name'];
                $testimonialsList[$i]['surname'] = $row['surname'];
                $testimonialsList[$i]['testimonial'] = $row['testimonial'];
                $i++;
            }
        }


        return $testimonialsList;
    }

    public static function createTestimonial($options) {
        $db = Db::getConnection();

        $sql = 'INSERT INTO testimonials ' .
            '(name, surname, testimonial) ' .
            'VALUES ' .
            '(:name, :surname, :testimonial) ';

        $result = $db->prepare($sql);
        $result->bindParam(':name', $options['name'], PDO::PARAM_STR);
        $result->bindParam(':surname', $options['surname'], PDO::PARAM_STR);
        $result->bindParam(':testimonial', $options['testimonial'], PDO::PARAM_STR);

        if($result->execute()) {
            return $db->lastInsertId();
        }
        return 0;

    }

    public static function getTestimonialById($id)
    {
        $id = intval($id);

        if ($id) {
            $db = Db::getConnection();

            $result = $db->query('SELECT * FROM testimonials WHERE id=' . $id);
            $result->setFetchMode(PDO::FETCH_ASSOC);

            return $result->fetch();
        }
    }

    public static function deleteTestimonialById($id) {
        $db = Db::getConnection();
        $sql = 'DELETE FROM testimonials WHERE id = :id';
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        return $result->execute();
    }

    public static function updateTestimonialById($id, $options) {
        $db = Db::getConnection();

        $sql = "UPDATE testimonials SET name = :name, surname = :surname, 
                testimonial = :testimonial WHERE id = :id";

        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->bindParam(':name', $options['name'], PDO::PARAM_STR);
        $result->bindParam(':surname', $options['surname'], PDO::PARAM_STR);
        $result->bindParam(':testimonial', $options['testimonial'], PDO::PARAM_STR);

        return $result->execute();
    }

}
?>